//
//  OmuraFile.h
//  OmuraAPI
//
//  Created by General on 12/13/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OmuraFile : NSObject

+ (NSDictionary *)setFileData:(NSData *)fileData
                  setFileName:(NSString *)fileName
                  setFileType:(NSString *)fileType;

@end
