//
//  OmuraFile.m
//  OmuraAPI
//
//  Created by General on 12/13/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import "OmuraFile.h"

@implementation OmuraFile

+ (NSDictionary *)setFileData:(NSData *)fileData
                  setFileName:(NSString *)fileName
                  setFileType:(NSString *)fileType {
    
    NSDictionary *fileDict = @{@"fileData":fileData,
                               @"fileName":fileName,
                               @"fileType":fileType
                               };
    
    return fileDict;
}


@end
