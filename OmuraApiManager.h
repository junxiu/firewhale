//
//  OmuraApiManager.h
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppAPI.h"

@interface OmuraApiManager : AppAPI

// Creates a note with attachment to a group {:id}
// Returns note ID and dictionary

- (void)createNote:(NSString *)note withAttachments:(NSArray *)attachments toGroup:(NSInteger)groupID withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock errorBlock:(void (^)(NSString *errorMessage))errorBlock;

- (void)attachFile:(NSData *)fileData withFileName:(NSString *)fileName andFileType:(NSString *)fileType toNote:(NSInteger)noteId;


// Refresh news feed with appointd group id

- (void)refreshNewsFeedInGroup:(NSInteger)groupID
           withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
                    errorBlock:(void (^)(NSString *errorMessage))errorBlock;
@end
