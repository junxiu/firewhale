# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this app about? ##

This is the future

## How do I get set up? ##

### Firstly, setup Cocoapods ( If you have not! ) ###
* Open "Terminal.app"
* Type "sudo gem update --system"
* Type "sudo gem install cocoapods"

### Then, clone the repo ###
* Open "Terminal.app"
* Type "git clone https://junxiu@bitbucket.org/junxiu/firewhale.git"

### And then.. download all the dependencies ###
* Type "git submodule init"
* Type "git submodule update"
* Type "pod setup"
* Type "pod install"

### Finally ###
* Type "open FireWhale.xcworkspace"

### NOTE! ###
Do not includes quote " " in terminal.app

## Problem? ##

There wont be any problem.