//
//  AppAPI.m
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import "AppAPI.h"

@interface AppAPI ()

@end

@implementation AppAPI

+ (id) sharedInstance {
    static id sharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

- (AFHTTPRequestOperation *)requestWithPath:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                                     method:(AFRequestMethod)method
                                 sslEnabled:(BOOL)sslEnabled
                            completionBlock:(void (^)(NSMutableDictionary *responseDict))completionBlock
                                 errorBlock:(void (^)(NSString *errorMessage))errorBlock {
    
    if (parameters == nil) {
        parameters = @{};
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    [mutableParameters setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"uuid"];
    [mutableParameters setObject:@"ios" forKey:@"platform"];
    [mutableParameters setObject:[UIDevice currentDevice].model forKey:@"device_type"];
    [mutableParameters setObject:[UIDevice currentDevice].systemVersion forKey:@"osversion"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *urlStr = [NSString stringWithFormat:@"http%@://%@/%@/%@",sslEnabled ? @"s" : @"",APP_HOSTNAME,VERSION,path];
    
    NSString *methodStr = method == AFRequestMethodGet ? @"GET" : (method == AFRequestMethodPost ? @"POST" : (method == AFRequestMethodPut ? @"PUT" : @"DELETE"));
    
    void (^success)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.responseData) {
            NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:(NSJSONReadingAllowFragments | NSJSONReadingMutableContainers) error:nil];
#ifdef DEBUG
            NSLog(@"RESPONSE RECEIVE FROM:\n -- Path: /%@\n -- Token: %@\n -- Method: %@\n -- Params: %@\n -- Result: %@",path,[KKKeychain getStringForKey:@"access_token"],methodStr,parameters,jsonDict);
#else
            NSLog(@"RESPONSE RECEIVE FROM: %@ %lu bytes",path,(unsigned long)operation.responseData.length);
#endif
            completionBlock(jsonDict);
        } else {
            NSLog(@"RESPONSE is empty.");
            errorBlock(@"RESPONSE is empty.");
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    };
    
    void (^failure)(AFHTTPRequestOperation *, NSError *) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        if (operation.responseData) {
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
            NSLog(@"ERROR FROM:\n -- Path: /%@\n -- Token: %@\n -- Method: %@\n -- Params: %@\n -- Result: %@\n -- Error: %@",path,[KKKeychain getStringForKey:@"access_token"],methodStr,parameters,jsonDict,error);
            errorBlock([jsonDict safeObjectForKey:@"error"]);
        } else {
            NSLog(@"ERROR RESPONSE is empty.");
            errorBlock(@"ERROR RESPONSE is empty.");
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    };
    
    switch (method) {
        case AFRequestMethodGet:{
            return [manager GET:urlStr parameters:parameters success:success failure:failure];
        }break;
        case AFRequestMethodPost:{
            return [manager POST:urlStr parameters:parameters success:success failure:failure];
        }break;
        case AFRequestMethodPut:{
            return [manager PUT:urlStr parameters:parameters success:success failure:failure];
        }break;
        case AFRequestMethodDelete:{
            return [manager DELETE:urlStr parameters:parameters success:success failure:failure];
        }break;
        default:{
            return [manager GET:urlStr parameters:parameters success:success failure:failure];
        }break;
    }
    
}

@end
