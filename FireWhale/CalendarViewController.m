//
//  CalendarViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/25/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "CalendarViewController.h"
#import "CalendarCollectionViewCell.h"
#import "Chameleon.h"
#import "iOS-UtilitiesClasses.h"
#import "CalendarTableViewCell.h"

@interface CalendarViewController () <JTCalendarDataSource, UITableViewDataSource, UITableViewDelegate>
{
    CAGradientLayer *maskLayer;
}
@end

@implementation CalendarViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.calendar = [JTCalendar new];
    
    [self.calendar setContentView:self.calendarContentView];
    [self.calendar setMenuMonthsView:self.calendarMenuView];
    [self.calendar setDataSource:self];

    self.calendar.calendarAppearance.calendar.firstWeekday = 2; // Monday
    self.calendar.calendarAppearance.ratioContentMenu = 1;
    self.calendar.calendarAppearance.menuMonthTextColor = [UIColor whiteColor];
    self.calendar.calendarAppearance.dayCircleColorSelected = [UIColor blueColor];
    self.calendar.calendarAppearance.dayTextColorSelected = [UIColor whiteColor];
    self.calendar.calendarAppearance.menuMonthTextFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17];
    self.calendar.calendarAppearance.isWeekMode = YES;
    self.calendar.calendarAppearance.dayTextFont = [UIFont fontWithName:@"HelveticaNeue" size:17];
    self.calendar.calendarAppearance.weekDayTextFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
    self.calendar.calendarAppearance.weekDayTextColor = [UIColor colorWithWhite:1 alpha:0.4f];
    self.calendar.calendarAppearance.weekDayFormat = JTCalendarWeekDayFormatSingle;
    self.calendar.calendarAppearance.dayTextColor = [UIColor colorWithWhite:1 alpha:1];
    self.calendar.calendarAppearance.dayCircleColorToday =  [UIColor colorWithRed:0.898 green:0.302 blue:0.510 alpha:1];
    self.calendar.calendarAppearance.dayCircleColorSelected = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1];
    self.calendar.calendarAppearance.dayDotColor = [UIColor colorWithRed:0.165 green:0.608 blue:0.922 alpha:1];

    [self.calendar reloadAppearance];
    
    self.calendarContainerView.layer.cornerRadius = 3;

    self.addButton.layer.cornerRadius = self.addButton.frame.size.height/2;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.calendar reloadData]; // Must be call in viewDidAppear
}

- (BOOL)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date
{
    if ([date isEqual:[NSDate date]]) {
        return YES;
    }
    return NO;
}

- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date
{
    NSLog(@"%@", date);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CalendarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CalendarTableViewCell" forIndexPath:indexPath];
    cell.tagView.backgroundColor = [UIColor randomFlatColor];
    [cell.titleLabel setFrameWidth:cell.frame.size.width - 98];
    switch (indexPath.row) {
        case 0:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.996 green:0.773 blue:0.475 alpha:1];
            cell.titleLabel.text = @"Meet Gabriel and Venus to discuss about Saleswhale";
            cell.startLabel.text = @"2:03 pm\n3:32 pm";
            break;
        case 1:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.996 green:0.678 blue:0.318 alpha:1];
            cell.titleLabel.text = @"Todo:Prepare mockup for Better Adam app v2.0";
                        cell.startLabel.text = @"4:00 pm\n5:40 pm";
            break;
        case 2:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.922 green:0.310 blue:0.510 alpha:1];
            cell.titleLabel.text = @"Send in report to Mr.Baymax lawyer firm";
                        cell.startLabel.text = @"7:05 pm\n7:30 pm";
            break;
        case 3:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.804 green:0.294 blue:0.486 alpha:1];
            cell.titleLabel.text = @"Discuss Avengers Age of Ultron sequel with Nolan";
                        cell.startLabel.text = @"10:00 pm\n11:15 pm";
            break;
        case 4:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.584 green:0.322 blue:0.702 alpha:1];
            cell.titleLabel.text = @"Interview with Robin Williams family regarding their loss.";
                        cell.startLabel.text = @"12:14 am\n3:30 am";
            break;
        case 5:
            //
            cell.tagView.backgroundColor = [UIColor colorWithRed:0.678 green:0.345 blue:0.737 alpha:1];
            cell.titleLabel.text = @"Go out and get a life";
                        cell.startLabel.text = @"4:00 am\n7:30 pm";
            break;
            
        default:
            break;
    }
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    maskLayer.position = CGPointMake(0, scrollView.contentOffset.y + scrollView.contentInset.top);
    [CATransaction commit];
}

#pragma mark - Methods

- (IBAction)close:(id)sender {
    [UIView animateWithDuration:0.4f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.contentView setFrameOriginY: - 12];
                     } completion:^(BOOL finished) {
                     }];
    
    
    [UIView animateWithDuration:0.3f
                          delay:0.2f
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.contentView setFrameOriginY:self.view.frame.size.height];
                         [self.backgroundImageView setAlpha:1];
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.35f
                          delay:0.3f
         usingSpringWithDamping:0.8f
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view viewWithTag:999].alpha = 0;
                     } completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
}

@end
