//
//  PostViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "PostViewController.h"
#import "PostCollectionViewCell.h"
#import "iOS-UtilitiesClasses.h"

@interface PostViewController () <UICollectionViewDelegate, UICollectionViewDataSource> {
    CAGradientLayer *maskLayer;
    BOOL dismissing;
}

@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!maskLayer)
    {
        maskLayer = [CAGradientLayer layer];
        
        UIColor *outerColor = [UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:1];
        UIColor *innerColor = [UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:0];
        
        maskLayer.colors = [NSArray arrayWithObjects:(id)innerColor.CGColor,
                            (id)innerColor.CGColor, (id)innerColor.CGColor, (id)outerColor.CGColor, nil];
        maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.2],
                               [NSNumber numberWithFloat:0.65],
                               [NSNumber numberWithFloat:1.0], nil];
        
        maskLayer.bounds = CGRectMake(0, 0,
                                      self.collectionView.frame.size.width,
                                      self.collectionView.frame.size.height);
        maskLayer.anchorPoint = CGPointZero;
        
        [self.collectionView.layer addSublayer:maskLayer];
    }
    
    self.replyButton.layer.cornerRadius = CGRectGetHeight(self.replyButton.bounds) / 2;
    self.replyButton.backgroundColor = [UIColor colorWithRed:0.996 green:0.596 blue:0.149 alpha:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    maskLayer.position = CGPointMake(0, scrollView.contentOffset.y + scrollView.contentInset.top);
    [CATransaction commit];
    
    if (scrollView.contentOffset.y <= -108 && !dismissing) {
        // Dismiss
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        dismissing = YES;
        [UIView animateWithDuration:0.25f
                              delay:0
             usingSpringWithDamping:1
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.collectionView.alpha = 0;
                             [self.blurBackgroundImageView setAlpha:0];
                             [self.backgroundImageView setAlpha:1];
                             [self.replyButton setAlpha:0];
                         } completion:^(BOOL finished) {
                             [self dismissViewControllerAnimated:NO completion:^{
                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"FEED_DID_DRAG" object:nil userInfo:nil];
                             }];
                         }];
    }
    
    if (scrollView.isDragging && !dismissing) {
        CGFloat alphaDown = (64 + (scrollView.contentOffset.y + scrollView.contentInset.top))/64;
        CGFloat alphaUp = 1 - alphaDown;
        
        [self.collectionView setAlpha:alphaDown];
        [self.replyButton setAlpha:alphaDown];
        [self.blurBackgroundImageView setAlpha:alphaDown * 1.5f];
        [self.backgroundImageView setAlpha:alphaUp * 2.0f];
    }
    else {
        if (!dismissing) {
            [UIView animateWithDuration:0.6f
                                  delay:0
                 usingSpringWithDamping:1
                  initialSpringVelocity:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.collectionView.alpha = 1;
                                 [self.blurBackgroundImageView setAlpha:1];
                                 [self.backgroundImageView setAlpha:0];
                                 [self.replyButton setAlpha:1];
                             } completion:^(BOOL finished) {
                             }];
        }
    }
    
    CGFloat backgroundImageOrigin = -64;
    CGFloat backgroundImageZeroOut = -(scrollView.contentInset.top + scrollView.contentOffset.y) + backgroundImageOrigin;
    
    if ((scrollView.contentInset.top + scrollView.contentOffset.y) < 0) {
        if (self.blurBackgroundImageView.frame.origin.y < 0) {
            [self.blurBackgroundImageView setFrameOriginY:backgroundImageZeroOut];
            [self.backgroundImageView setFrameOriginY:backgroundImageZeroOut];
        }
        else {
            [self.blurBackgroundImageView setFrameOriginY:0];
            [self.backgroundImageView setFrameOriginY:0];
        }
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 1.001);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PostCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PostCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
}

@end
