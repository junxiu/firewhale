//
//  BlurVerticalSegue.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "BlurVerticalSegue.h"
#import "iOS-UtilitiesClasses.h"
#import "RootViewController.h"

@implementation BlurVerticalSegue

- (void)perform {
    // Init
    
    UIViewController *svc = self.sourceViewController;
    RootViewController *rvc = (RootViewController *)svc.parentViewController;
    UIViewController *dvc = self.destinationViewController;
    UIImage *screenshotImage = [[[UIApplication sharedApplication] delegate] window].screenshot;
    UIImageView *screenshotImageView = [[UIImageView alloc] initWithImage:[screenshotImage applyTintEffectWithColor:[UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:1]]];
    __block BOOL navBarZeroOrigin;
    
    // Add on setup
    
    screenshotImageView.alpha = 0;
    
    // Adding subviews
    
    [rvc.view addSubview:screenshotImageView];
    [dvc.view setFrameOriginY:CGRectGetMaxY(rvc.view.bounds)];
    [rvc.view addSubview:dvc.view];
    
    // Animations
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         screenshotImageView.alpha = 1;
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.45f
                          delay:0.2f
         usingSpringWithDamping:0.8f
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [dvc.view setFrameOriginY:0];
                         [svc.view setFrameOriginY:svc.view.frame.origin.y - 64];
                         
                         if (rvc.addButton.frame.origin.y < rvc.addButton.superview.frame.size.height) {
                             [rvc.addButton setFrameOriginY:rvc.addButton.frame.origin.y - 64];
                             navBarZeroOrigin = NO;
                         }
                         else {
                             navBarZeroOrigin = YES;
                         }
                         
                         if (rvc.navBarContainerView.frame.origin.y >= 0) {
                             [rvc.navBarContainerView setFrameOriginY:-64];
                         }
                         
                         [screenshotImageView setFrameOriginY:screenshotImageView.frame.origin.y - 64];
                     } completion:^(BOOL finished) {
                         [rvc presentViewController:dvc animated:NO completion:^{
                             ((UIImageView *)[dvc.view viewWithTag:9999]).image = screenshotImageView.image;
                             ((UIImageView *)[dvc.view viewWithTag:9998]).image = screenshotImage;
                             dvc.view.backgroundColor = [UIColor colorWithRed:0.184 green:0.188 blue:0.235 alpha:1];
                             
                             [screenshotImageView removeFromSuperview];
                             [svc.view setFrameOriginY:0];
                             
                             if (navBarZeroOrigin) {
                                 [rvc.navBarContainerView setFrameOriginY:-64];
                             }
                             else {
                                 [rvc.navBarContainerView setFrameOriginY:0];
                             }
                             
                             if (rvc.addButton.frame.origin.y < rvc.addButton.superview.frame.size.height) {
                                 [rvc.addButton setFrameOriginY:rvc.addButton.frame.origin.y + 64];
                             }
                         }];
                     }];
}
@end
