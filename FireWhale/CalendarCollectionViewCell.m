//
//  CalendarCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/24/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "CalendarCollectionViewCell.h"

@implementation CalendarCollectionViewCell

-(void)awakeFromNib {
    [self.calendar setContentView:self.calendarView];
    [self.calendarView setCurrentDate:[NSDate date]];
    [self.calendar reloadData];
}

@end
