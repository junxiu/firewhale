//
//  MenuTableViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/1/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "MenuTableViewController.h"
#import "JCRBlurView.h"
#import "Chameleon.h"

@interface MenuTableViewController ()

@end

@implementation MenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)additionalSetup {
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.height/2;
    self.profileImageView.layer.borderWidth = 2;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:234/255.f green:235/255.f blue:236/255.f alpha:0.3f].CGColor;
    self.searchContentView.layer.cornerRadius = 4;
    self.separatorView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.separatorView.bounds andColors:@[[UIColor colorWithRed:54/255.f green:55/255.f blue:67/255.f alpha:0],[UIColor colorWithRed:54/255.f green:55/255.f blue:67/255.f alpha:0.3f],[UIColor colorWithRed:54/255.f green:55/255.f blue:67/255.f alpha:1]]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    [[cell.contentView viewWithTag:999] setBackgroundColor:[UIColor colorWithRed:234/255.f green:235/255.f blue:236/255.f alpha:0.03f]];
    if (indexPath.row == 0) {
        [[cell.contentView viewWithTag:999] setBackgroundColor:[UIColor colorWithRed:0.996 green:0.596 blue:0.149 alpha:1]];
    }
    [cell.contentView viewWithTag:999].layer.cornerRadius = 4;
    return cell;
    
}

@end
