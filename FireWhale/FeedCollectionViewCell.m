//
//  FeedCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "FeedCollectionViewCell.h"

@implementation FeedCollectionViewCell

- (void)awakeFromNib {
    if (self.subContentView.layer.cornerRadius == 0) {
        self.subContentView.layer.cornerRadius = 3;
    }
    if (self.contentImageView.layer.cornerRadius == 0) {
        self.contentImageView.layer.cornerRadius = 3;
    }
}
@end
