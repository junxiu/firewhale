//
//  NavBarViewController.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCRBlurView.h"

@interface NavBarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@property (weak, nonatomic) IBOutlet UIButton *notifButton;
@property (weak, nonatomic) IBOutlet UIButton *profButton;
@property (strong, nonatomic) IBOutlet JCRBlurView *navBlurView;
- (IBAction)showSideBar:(id)sender;
@end
