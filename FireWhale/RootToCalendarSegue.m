//
//  RootToOverviewSegue.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "RootToCalendarSegue.h"
#import "iOS-UtilitiesClasses.h"
#import "RootViewController.h"
#import "PostViewController.h"
#import "CalendarViewController.h"
#import "Chameleon.h"

@implementation RootToCalendarSegue

- (void)perform {
    // Init
    
    UIViewController *svc = self.sourceViewController;
    RootViewController *rvc = (RootViewController *)svc.parentViewController;
    CalendarViewController *dvc = (CalendarViewController *)self.destinationViewController;
    UIImage *screenshotImage = [[[UIApplication sharedApplication] delegate] window].screenshot;
    UIImageView *screenshotImageView = [[UIImageView alloc] initWithImage:[screenshotImage applyTintEffectWithColor:[UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:1]]];
    
    UIView *gradientView = [[UIView alloc] initWithFrame:dvc.view.bounds];
    gradientView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:gradientView.bounds andColors:@[[UIColor colorWithRed:0.184 green:0.188 blue:0.235 alpha:1],[UIColor colorWithRed:0.184 green:0.188 blue:0.235 alpha:0.5]]];
    
    // Add on setup
    
    screenshotImageView.alpha = 0;
    gradientView.alpha = 0;
    
    // Adding subviews
    
    [rvc.view addSubview:screenshotImageView];
    [rvc.view addSubview:gradientView];
    [dvc.view setFrameOriginX:-CGRectGetMaxX(rvc.view.bounds)];
    [rvc.view addSubview:dvc.view];
    
    
    // Animations
    
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         screenshotImageView.alpha = 1;
                         gradientView.alpha = 1;
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.45f
                          delay:0.2f
         usingSpringWithDamping:0.8f
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [dvc.view setFrameOriginX:0];
                         
                     } completion:^(BOOL finished) {
                         [rvc presentViewController:dvc animated:NO completion:^{
                             UIView *screenshotContentView = [[UIView alloc] initWithFrame:dvc.view.bounds];
                             UIView *secondGradientView = [[UIView alloc] initWithFrame:dvc.view.bounds];
                             secondGradientView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:secondGradientView.bounds andColors:@[[UIColor colorWithRed:0.184 green:0.188 blue:0.235 alpha:1],[UIColor colorWithRed:0.184 green:0.188 blue:0.235 alpha:0.5]]];
                             UIImageView *secondScreenShotImageView = [[UIImageView alloc] initWithImage:screenshotImageView.image];
                             screenshotContentView.tag = 999;
                             
                             dvc.backgroundImageView.image = screenshotImage;
                             [screenshotContentView addSubview:secondScreenShotImageView];
                             [screenshotContentView addSubview:secondGradientView];
                             [dvc.view insertSubview:screenshotContentView aboveSubview:dvc.backgroundImageView];
                             
                             [screenshotImageView removeFromSuperview];
                             [gradientView removeFromSuperview];
                             [svc.view setFrameOriginY:0];
                         }];
                     }];
}

@end
