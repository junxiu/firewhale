//
//  CalendarTableViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/25/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "CalendarTableViewCell.h"

@implementation CalendarTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.tagContentView.layer.cornerRadius = 3;
    self.tagView.layer.cornerRadius = self.tagView.frame.size.height/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsZero;
}
@end
