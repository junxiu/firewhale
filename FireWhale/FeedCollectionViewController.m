//
//  FeedCollectionViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "FeedCollectionViewController.h"
#import "FeedCollectionViewCell.h"
#import "AttachmentCollectionViewCell.h"
#import "DataSource.h"

#import "JCRBlurView.h"

@interface FeedCollectionViewController () {
    BOOL bootAnimated;
    CAGradientLayer *maskLayer;
    NSArray *dataSourceArray;
}

@property (nonatomic) UICollectionViewCell *selectedCell;

@end

@implementation FeedCollectionViewController

static NSString * const reuseIdentifier = @"FeedCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void)additionalSetup {
    dataSourceArray = [[DataSource sharedInstance] downloadData];
}

- (NSArray *)customCellSizeArray {
    NSMutableArray *customCellSizeMutArray = [NSMutableArray array];
    CGFloat fullwidth = CGRectGetWidth(self.view.bounds) - 10;
    CGFloat threeQuarterWidth = round(fullwidth / 2);
    CGFloat quarterWidth = fullwidth - threeQuarterWidth;
    CGFloat height = round(fullwidth / 1.618 / 1.3);
    int d = -1;
    for (int i = 0; i < 100; i ++) {
        d++;
        switch (d) {
            case 1:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(threeQuarterWidth, height)]];
                break;
            case 2:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(quarterWidth, height)]];
                break;
            case 3:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(fullwidth, height)]];
                break;
            case 4:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(quarterWidth, height)]];
                break;
            case 5:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(threeQuarterWidth, height)]];
                break;
            case 6:
                [customCellSizeMutArray addObject:[NSValue valueWithCGSize:CGSizeMake(fullwidth, height)]];
                break;
                
            default:
                break;
        }
        if (d == 6) {
            d = -1;
        }
    }
    return [NSArray arrayWithArray:customCellSizeMutArray];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataSourceArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FeedCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (!collectionView.isDragging) {
        if (!bootAnimated) {
            cell.subContentView.alpha = 0;
            cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y - 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
            cell.subContentView.transform = CGAffineTransformMakeScale(0.96f, 0.96f);
            
            [UIView animateWithDuration:1.f
                                  delay:indexPath.item * 0.05 + 0.15f
                 usingSpringWithDamping:0.5
                  initialSpringVelocity:20
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 cell.subContentView.alpha = 1;
                                 cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y + 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
                                 cell.subContentView.transform = CGAffineTransformIdentity;
                             } completion:^(BOOL finished) {
                                 bootAnimated = YES;
                             }];
        }
    }
    
    NSString *sourceStr, *contentStr, *titleStr, *authorStr, *dueStr;
    NSMutableAttributedString *contentMutAttStr, *subContentMutAttStr;
    
    sourceStr = [[dataSourceArray objectAtIndex:indexPath.item] safeObjectForKey:@"source"];
    authorStr = [[dataSourceArray objectAtIndex:indexPath.item] safeObjectForKey:@"author"];
    contentStr = [[dataSourceArray objectAtIndex:indexPath.item] safeObjectForKey:@"content"];
    dueStr = [[dataSourceArray objectAtIndex:indexPath.item] safeObjectForKey:@"due"];
    
    cell.subContentLabel.text = @"";
    cell.subContentLabel.attributedText = nil;
    
    if (self.view.frame.size.height > 667) {
        cell.contentLabel.numberOfLines = 5;
        [cell.contentLabel setFrameHeight:106];
    }
    else if (self.view.frame.size.height > 568) {
        cell.contentLabel.numberOfLines = 4;
        [cell.contentLabel setFrameHeight:90];
    }
    else {
        cell.contentLabel.numberOfLines = 3;
        [cell.contentLabel setFrameHeight:71];
    }
    
    [cell.contentLabel setFrameOriginY:26];
    
    if ([[[dataSourceArray objectAtIndex:indexPath.item] safeObjectForKey:@"type"] integerValue] == kNote) {
        NSString *sourceAndAuthorStr = [NSString stringWithFormat:@"%@ by %@",sourceStr,authorStr];
        subContentMutAttStr = [[NSMutableAttributedString alloc] initWithString:sourceAndAuthorStr];

        [subContentMutAttStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, sourceStr.length + 4)];
        [subContentMutAttStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.118 green:0.698 blue:0.992 alpha:1] range:NSMakeRange(sourceStr.length + 4, authorStr.length)];
        
        [subContentMutAttStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12] range:NSMakeRange(0, sourceStr.length + 4)];
        [subContentMutAttStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12] range:NSMakeRange(sourceStr.length + 4, authorStr.length)];
        
        cell.contentLabel.text = contentStr;
    }
    else {
        NSString *dueAndContentStr = [NSString stringWithFormat:@"%@\n%@",contentStr,dueStr];
        contentMutAttStr = [[NSMutableAttributedString alloc] initWithString:dueAndContentStr];
        
        [contentMutAttStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, contentStr.length)];
        [contentMutAttStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.996 green:0.596 blue:0.149 alpha:1] range:NSMakeRange(contentStr.length + 1, dueStr.length)];
        
        [contentMutAttStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15] range:NSMakeRange(0, contentStr.length)];
        [contentMutAttStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15] range:NSMakeRange(contentStr.length + 1, dueStr.length)];
        
        cell.contentLabel.attributedText = contentMutAttStr;
    }
    
    cell.subContentLabel.text = authorStr;
    cell.timeLabel.text = [NSDate shortTimeAgoSinceDate:[NSDate dateWithTimeIntervalSinceNow:120 * (indexPath.item + 1)]];

#warning message -- test only
    if ((indexPath.item %4)==0 || (indexPath.item %6)==0 || (indexPath.item %2)==0) {
        AttachmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttachmentCollectionViewCell" forIndexPath:indexPath];
        
        if (!collectionView.isDragging) {
            if (!bootAnimated) {
                cell.subContentView.alpha = 0;
                cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y - 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
                cell.subContentView.transform = CGAffineTransformMakeScale(0.96f, 0.96f);
                
                [UIView animateWithDuration:0.65f
                                      delay:indexPath.item * 0.05 + 0.15f
                     usingSpringWithDamping:0.5
                      initialSpringVelocity:20
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     cell.subContentView.alpha = 1;
                                     cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y + 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
                                     cell.subContentView.transform = CGAffineTransformIdentity;
                                 } completion:^(BOOL finished) {
                                     bootAnimated = YES;
                                 }];
            }
        }
        
        if ((indexPath.item %7)==0) {
            cell.attachmentImageView.image = [UIImage imageNamed:@"star1"];
            cell.contentLabel.text = @"EPISODE VII";
            cell.subContentLabel.text = @"Paulius";
        }
        else if((indexPath.item %4)==0){
            cell.attachmentImageView.image = [UIImage imageNamed:@"star3.jpg"];
            cell.contentLabel.text = @"Stormtrooper - Polygon Pixel";
            cell.subContentLabel.text = @"Matthew Reilly";
        }
        else if((indexPath.item %2)==0) {
            cell.attachmentImageView.image = [UIImage imageNamed:@"star2"];
            cell.contentLabel.text = @"Millennium Falcon // Flat!";
            cell.subContentLabel.text = @"Joshua Lepley";
        }
        if (self.view.frame.size.height > 667) {
            cell.contentLabel.numberOfLines = 5;
            [cell.contentLabel setFrameHeight:106];
        }
        else if (self.view.frame.size.height > 568) {
            cell.contentLabel.numberOfLines = 4;
            [cell.contentLabel setFrameHeight:90];
        }
        else {
            cell.contentLabel.numberOfLines = 3;
            [cell.contentLabel setFrameHeight:71];
        }
        
        [cell.contentLabel setFrameOriginY:26];
        return cell;
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSMutableDictionary *infoMutDict = [NSMutableDictionary dictionary];
    [infoMutDict setObject:scrollView forKey:@"scrollView"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SCROLLVIEW_DID_END_DRAGGING" object:nil userInfo:infoMutDict];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView.superview];
    
    if (scrollView.isDragging) {
        if(translation.y > 0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FEED_DID_DRAG" object:nil userInfo:nil];
            
            [UIView animateWithDuration:0.35f
                             animations:^{
                                 maskLayer.opacity = 1;
                             }];
        }
        else {
            NSMutableDictionary *infoMutDict = [NSMutableDictionary dictionary];
            [infoMutDict setObject:[NSValue valueWithCGPoint:scrollView.contentOffset] forKey:@"scrollViewContentOffset"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FEED_DID_SCROLL" object:nil userInfo:infoMutDict];
            
            [UIView animateWithDuration:0.35f
                             animations:^{
                                 maskLayer.opacity = 0;
                             }];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [[[self customCellSizeArray] objectAtIndex:indexPath.item] CGSizeValue];
    return CGSizeMake(self.view.frame.size.width-10, (self.view.frame.size.width-10)/2.2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = [collectionView cellForItemAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    FeedCollectionViewCell *cell = (FeedCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.subContentView.transform = CGAffineTransformMakeScale(0.96f, 0.96f);
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    FeedCollectionViewCell *cell = (FeedCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        cell.subContentView.transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                     }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"seguePost"]) {
    }
}
 
@end
