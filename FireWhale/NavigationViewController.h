//
//  NavigationViewController.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/1/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCRBlurView.h"

@interface NavigationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *rootView;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIView *gradientView;
- (IBAction)overlayViewDidTapped:(UITapGestureRecognizer *)sender;
@end
