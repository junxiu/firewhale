//
//  CreateViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "CreateViewController.h"
#import "iOS-UtilitiesClasses.h"
#import "JCRBlurView.h"
#import "DropboxBrowserViewController.h"
#import "AttachmentViewController.h"
#import "MIBadgeButton.h"
#import "OmuraFile.h"

@interface CreateViewController () <UITextViewDelegate,DropboxBrowserDelegate,AttachmentViewControllerDeleate> {
    DropboxBrowserViewController *dropboxBrowser;
    AttachmentViewController *attachmentViewController;
}

@property (nonatomic) MIBadgeButton *attachmentButton;
@property (nonatomic) NSArray *attachments;

@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"AttachmentActionSheetSegue"]) {
        attachmentViewController = segue.destinationViewController;
        attachmentViewController.delegate = self;
    }
}

#pragma mark - AttachmentViewControllerDeleate

- (void)attachmentsDidModified:(NSArray *)newAttachments {
    self.attachments = newAttachments;
    [self.attachmentButton setBadgeString:[NSString stringWithFormat:@"%lu",(unsigned long)self.attachments.count]];
    
    if (newAttachments.count > 0) {
        self.attachmentButton.imageView.tintColor = [UIColor colorWithRed:0.906 green:0.290 blue:0.506 alpha:0.5f];
        [self.composeTextView.inputAccessoryView viewWithTag:9999].tintColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1];
    }
    else {
        self.attachmentButton.imageView.tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
        [self.composeTextView.inputAccessoryView viewWithTag:9999].tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
    }
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    NSString *text = textView.text;
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    _composePlaceholderLabel.alpha = 0;
    if (text.length == 0) {
        _composePlaceholderLabel.alpha = 1;
        textView.text = @"";
        
        [UIView animateWithDuration:0.1f
                         animations:^{
                             [textView.inputAccessoryView viewWithTag:9999].tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
                         }];
    }
    else {
        if ([[textView.inputAccessoryView viewWithTag:9999].tintColor isEqual:[UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f]]) {
            [UIView animateWithDuration:0.1f
                             animations:^{
                                 [textView.inputAccessoryView viewWithTag:9999].tintColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1];
                                 
                                 [textView.inputAccessoryView viewWithTag:9999].transform = CGAffineTransformMakeScale(0.85f, 0.85f);
                             }];
            
            [UIView animateWithDuration:0.35f
                                  delay:0.1
                 usingSpringWithDamping:0.5f
                  initialSpringVelocity:20
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 [textView.inputAccessoryView viewWithTag:9999].transform = CGAffineTransformIdentity;
                                 //
                             } completion:^(BOOL finished) {
                                 //
                             }];
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ( [text isEqualToString:@"\n"] ) {
        if (textView.text.length == 0) {
            [self shakeView:self.composePlaceholderLabel];
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}

- (void)shakeView:(UIView *)view{
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.05];
    [shake setRepeatCount:3];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:CGPointMake(view.center.x - 5,view.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:CGPointMake(view.center.x + 5, view.center.y)]];
    [view.layer addAnimation:shake forKey:@"position"];
}

#pragma mark - Methods

- (void)send {

    __block NSMutableArray *files = [NSMutableArray array];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSDictionary *attachmentDict in self.attachments) {
            
            NSData *attachmentData;
            NSString *attachmentType;
            NSString *attachmentName;
            
            if ([attachmentDict safeObjectForKey:@"db"]) {
                DBMetadata *dbAttachment = (DBMetadata *)[attachmentDict safeObjectForKey:@"db"];
                attachmentName = dbAttachment.filename;
                attachmentData = [NSKeyedArchiver archivedDataWithRootObject:dbAttachment];
                attachmentType = @"application/json";
            }
            else if ([attachmentDict safeObjectForKey:@"local"]) {
                NSDictionary *localAttachment = [attachmentDict safeObjectForKey:@"local"];
                
                if ([[localAttachment safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
                    attachmentType = @"image/jpeg";
                    attachmentData = UIImageJPEGRepresentation([localAttachment safeObjectForKey:UIImagePickerControllerOriginalImage], 1);
                    attachmentName = [localAttachment objectForKey:@"UIImagePickerControllerReferenceURL"];
                }
                else if ([[localAttachment safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
                    attachmentType = @"video/mp4";
                    attachmentData = [NSData dataWithContentsOfURL:[localAttachment safeObjectForKey:UIImagePickerControllerMediaURL]];
                    attachmentName = [localAttachment objectForKey:@"UIImagePickerControllerMediaURL"];
                }
            }
            
            NSDictionary *dictToUpload = [OmuraFile setFileData:attachmentData
                                                    setFileName:attachmentName
                                                    setFileType:attachmentType];
            
            [files addObject:dictToUpload];
            
        }
        
        [[OmuraApiManager sharedInstance] createNote:self.composeTextView.text
                                     withAttachments:files
                                             toGroup:1
                                 withCompletionBlock:^(NSDictionary *responseDict) {
                                     //
                                 } errorBlock:^(NSString *errorMessage) {
                                     //
                                 }];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [self dismiss:nil];
        });
    });
    
}

- (void)loadDropboxBrowser {
    dropboxBrowser = [[DropboxBrowserViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:dropboxBrowser];
    dropboxBrowser.deliverDownloadNotifications = NO;
    dropboxBrowser.shouldDisplaySearchBar = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)loadAttachmentView {
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.overlayView.alpha = 0.65f;
                         [self.attachmentView setFrameOriginY:self.view.frame.size.height - self.attachmentView.frame.size.height];
                     } completion:^(BOOL finished) {
                     }];
}

- (void)dismissAttachmentView {
    [self performBlock:^{
        [self.composeTextView becomeFirstResponder];
        [self textViewAccButtonSelected:(UIButton *)[self.composeTextView.inputAccessoryView viewWithTag:9997]];
    } afterDelay:0.05];
    
    [UIView animateWithDuration:0.45f
                          delay:0
         usingSpringWithDamping:0.85f
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.overlayView.alpha = 0;
                         [self.attachmentView setFrameOriginY:self.view.frame.size.height + 12];
                     } completion:^(BOOL finished) {
                     }];
}

- (IBAction)overlayViewDidTapped:(id)sender {
    [self dismissAttachmentView];
}

- (IBAction)dismiss:(id)sender {
    [self.view endEditing:YES];
    
    [UIView animateWithDuration:0.4f
                          delay:0.1
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.navBarView setFrameOriginY: - 12];
                         [self.contentView setFrameOriginY:self.contentView.frame.origin.y - 12];
                     } completion:^(BOOL finished) {
                     }];
    
    
    [UIView animateWithDuration:0.3f
                          delay:0.3f
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.navBarView setFrameOriginY:self.view.frame.size.height];
                         [self.contentView setFrameOriginY:self.view.frame.size.height + self.navBarView.frame.size.height];
                         [self.backgroundImageView setAlpha:1];
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.35f
                          delay:0.4f
         usingSpringWithDamping:0.8f
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view viewWithTag:999].alpha = 0;
                     } completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
}

- (UIView *)customAccessoryView {
    CGFloat paddingSides = 4;
    CGFloat paddingTopBtm = 5;
    
    JCRBlurView *aView = [[JCRBlurView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    aView.blurTintColor = [UIColor whiteColor];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5f)];
    separatorView.backgroundColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.1f];
    [aView addSubview:separatorView];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [sendButton setFrame:CGRectMake(aView.frame.size.width -paddingSides - 50, paddingTopBtm + 1, 50, aView.frame.size.height - paddingTopBtm - paddingTopBtm)];
    sendButton.tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
    sendButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    sendButton.layer.cornerRadius = 4;
    sendButton.tag = 9999;
    [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    [aView addSubview:sendButton];

    UIButton *textButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [textButton setTitle:@"Aa" forState:UIControlStateNormal];
    [textButton setFrame:CGRectMake(paddingSides + 2, paddingTopBtm + 1, aView.frame.size.height - paddingTopBtm - paddingTopBtm, aView.frame.size.height - paddingTopBtm - paddingTopBtm)];
    textButton.tintColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1];
    textButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    textButton.layer.cornerRadius = 4;
    textButton.tag = 9997;
    [textButton addTarget:self action:@selector(textViewAccButtonSelected: ) forControlEvents:UIControlEventTouchUpInside];
    [aView addSubview:textButton];
    
    self.attachmentButton = [MIBadgeButton buttonWithType:UIButtonTypeSystem];
    [self.attachmentButton setFrame:CGRectMake( 2 * paddingSides + 68, paddingTopBtm + 1, aView.frame.size.height - paddingTopBtm - paddingTopBtm, aView.frame.size.height - paddingTopBtm - paddingTopBtm)];
    self.attachmentButton.imageView.tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
    [self.attachmentButton setBadgeEdgeInsets:UIEdgeInsetsMake(13, 0, 0, 13)];
    self.attachmentButton.layer.cornerRadius = 4;
    self.attachmentButton.tag = 9998;
    
    [self.attachmentButton setImage:[[UIImage imageNamed:@"attach"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.attachmentButton addTarget:self action:@selector(textViewAccButtonSelected: ) forControlEvents:UIControlEventTouchUpInside];
    [aView addSubview:self.attachmentButton];
    
    UIButton *tagButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [tagButton setFrame:CGRectMake(2 * paddingSides + 34, paddingTopBtm + 1, aView.frame.size.height - paddingTopBtm - paddingTopBtm, aView.frame.size.height - paddingTopBtm - paddingTopBtm)];
    tagButton.tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
    tagButton.layer.cornerRadius = 4;
    [tagButton setTitle:@"@" forState:UIControlStateNormal];
    tagButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    [tagButton addTarget:self action:@selector(textViewAccButtonSelected: ) forControlEvents:UIControlEventTouchUpInside];
    [aView addSubview:tagButton];
    
    [self textViewAccButtonSelected:textButton];
    return aView;
}

- (void)textViewAccButtonSelected:(UIButton *)button {
    if (button.tag == 9998) {
        [self loadAttachmentView];
    }
    else {
        [UIView animateWithDuration:0.1f
                         animations:^{
                             for (UIButton *_button in button.superview.subviews) {
                                 if (_button.tag == 9999) {
                                     continue;
                                 }
                                 else if ([_button isKindOfClass:[UIButton class]]) {
                                     _button.tintColor = [UIColor colorWithRed:0.192 green:0.188 blue:0.239 alpha:0.2f];
                                     _button.backgroundColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:0];
                                     if ([_button isEqual:button]) {
                                         _button.tintColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1];
                                         _button.backgroundColor = [UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:0.1f];
                                     }
                                 }
                             }
                         }];
    }
}

- (void)additionalSetup {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"ALL_SELECTED_FILES"];
    
    self.authorImageView.layer.cornerRadius = self.authorImageView.frame.size.height/2;
    self.authorImageView.layer.borderWidth = 2;
    self.authorImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.contentView.layer.cornerRadius = 4;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissAttachmentView)
                                                 name:@"DISMISS_ATTACHMENT_ACTION_SHEET" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadDropboxBrowser)
                                                 name:@"LOAD_DROPBOX_BROWSER" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    self.groupButton.layer.shadowColor = self.groupButton.tintColor.CGColor;
    self.groupButton.layer.shadowOffset = CGSizeZero;
    self.groupButton.layer.shadowRadius = 5;
    self.groupButton.layer.shadowOpacity = 0.35f;
    
    self.composeTextView.inputAccessoryView = [self customAccessoryView];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [self.composeTextView setFrameHeight:self.contentView.frame.size.height - kbSize.height + 25];
}

@end
