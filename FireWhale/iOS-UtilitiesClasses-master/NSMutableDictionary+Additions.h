//
//  NSMutableDictionary+Additions.h
//
//  Created by BuUuKeen on 28/6/14.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (Additions)

- (void)safeSetObject:(id)object forKey:(id)key;

@end
