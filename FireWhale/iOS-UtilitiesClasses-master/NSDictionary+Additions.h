//
//  NSDictionary+Additions.m
//
//  Thkeen

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

- (id)safeObjectForKey:(id)key;
- (id)notNilStringForKey:(id)key;

@end
