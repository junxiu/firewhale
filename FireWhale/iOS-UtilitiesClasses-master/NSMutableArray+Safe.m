//
//  NSMutableArray+Safe.m
//  myENV
//
//  Created by BuUuKeen on 28/6/14.
//  Copyright (c) 2014 National Environment Agency. All rights reserved.
//

#import "NSMutableArray+Safe.h"

@implementation NSMutableArray (Safe)

- (void)safeInsertObject:(id)anObject atIndex:(NSUInteger)index {
    if (anObject != nil && anObject != [NSNull null]) {
        [self insertObject:anObject atIndex:index];
    }
}

- (void)safeAddObject:(id)anObject {
    if (anObject != nil && anObject != [NSNull null]) {
        [self addObject:anObject];
    }
}

@end
