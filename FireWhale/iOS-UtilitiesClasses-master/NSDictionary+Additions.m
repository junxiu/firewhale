//
//  NSDictionary+Additions.m
//
//  Thkeen

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (id)safeObjectForKey:(id)key {
    id obj = [self objectForKey:key];
    return ([obj isEqual:[NSNull null]]) ? nil : obj;
}

- (id)notNilStringForKey:(id)key {
    id obj = [self objectForKey:key];
    if (obj == nil || [obj isEqual:[NSNull null]]) return @"";
    return obj;
}

@end
