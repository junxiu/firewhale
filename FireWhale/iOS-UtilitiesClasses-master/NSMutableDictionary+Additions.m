//
//  NSMutableDictionary+Additions.m
//
//  Created by BuUuKeen on 28/6/14.
//

#import "NSMutableDictionary+Additions.h"

@implementation NSMutableDictionary (Additions)


- (void)safeSetObject:(id)object forKey:(id)key {
    if (object != nil && object != [NSNull null]) {
        [self setObject:object forKey:key];
    }
    return;
}

@end
