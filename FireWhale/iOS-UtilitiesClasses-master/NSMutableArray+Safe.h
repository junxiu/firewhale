//
//  NSMutableArray+Safe.h
//  myENV
//
//  Created by BuUuKeen on 28/6/14.
//  Copyright (c) 2014 National Environment Agency. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Safe)

- (void)safeInsertObject:(id)anObject atIndex:(NSUInteger)index;
- (void)safeAddObject:(id)anObject;

@end
