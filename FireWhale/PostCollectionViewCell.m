//
//  PostCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "PostCollectionViewCell.h"

@implementation PostCollectionViewCell

- (void)awakeFromNib {
    if (self.subContentView.layer.cornerRadius == 0) {
        self.subContentView.layer.cornerRadius = 3;
    }
}

@end
