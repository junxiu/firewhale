//
//  DataSource.m
//  FireWhale
//
//  Created Jun Xiu Chan on 11/20/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "DataSource.h"

@implementation DataSource
+ (id)sharedInstance {
    static id sharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

- (NSArray *)getWorkspaces {
    NSMutableArray *workspaceArray = [NSMutableArray array];
    
    NSMutableArray *groupArray = [NSMutableArray array];
    [groupArray addObject:@"Epson Android POS Deal"];
    [groupArray addObject:@"Zalora Magazine Redesign Deal"];
    [groupArray addObject:@"NTU Augmented Reality App"];
    [groupArray addObject:@"Penetron International Deal"];
    
    NSMutableDictionary *workspaceDictionary = [NSMutableDictionary dictionary];
    [workspaceDictionary setObject:groupArray forKey:@"groups"];
    
    [workspaceArray addObject:workspaceDictionary];
    
    return workspaceArray;
}

- (NSArray *)downloadData {
    NSMutableArray *dataArray = [NSMutableArray array];

    NSString *contentString = @"";
    NSString *titleString = @"";
    NSString *sourceString = @"";
    NSString *authorString = @"";
    NSString *dueString = @"";
    NSMutableDictionary *dataDictionary;
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];

    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"リレーションシップ·インテリジェンスは、人々が人々とビジネスを行う、通信や顧客との事の上に滞在し、理解、計画の芸術であると一日の終わりをcolleagues.At 。でも、大企業は、顔のない実体ではありませんが、それらは単に人々のグループで構成されている。あなたが人々を治療する方法より多くの紹介をもっとお得な情報を閉じるとなっての鍵であると考えているのであれば、あなたは関係インテリジェンスが必要です。";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"ガブリエル";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];

    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Steve";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Fear is the path to the dark side…fear leads to anger…anger leads to hate...hate leads to suffering.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Gabriel";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"リレーションシップ·インテリジェンスは、人々が人々とビジネスを行う、通信や顧客との事の上に滞在し、理解、計画の芸術であると一日の終わりをcolleagues.At 。でも、大企業は、顔のない実体ではありませんが、それらは単に人々のグループで構成されている。あなたが人々を治療する方法より多くの紹介をもっとお得な情報を閉じるとなっての鍵であると考えているのであれば、あなたは関係インテリジェンスが必要です。";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"ガブリエル";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Steve";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Fear is the path to the dark side…fear leads to anger…anger leads to hate…hate leads to suffering.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Gabriel";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"リレーションシップ·インテリジェンスは、人々が人々とビジネスを行う、通信や顧客との事の上に滞在し、理解、計画の芸術であると一日の終わりをcolleagues.At 。でも、大企業は、顔のない実体ではありませんが、それらは単に人々のグループで構成されている。あなたが人々を治療する方法より多くの紹介をもっとお得な情報を閉じるとなっての鍵であると考えているのであれば、あなたは関係インテリジェンスが必要です。";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"ガブリエル";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Steve";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Fear is the path to the dark side…fear leads to anger…anger leads to hate…hate leads to suffering.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Gabriel";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"リレーションシップ·インテリジェンスは、人々が人々とビジネスを行う、通信や顧客との事の上に滞在し、理解、計画の芸術であると一日の終わりをcolleagues.At 。でも、大企業は、顔のない実体ではありませんが、それらは単に人々のグループで構成されている。あなたが人々を治療する方法より多くの紹介をもっとお得な情報を閉じるとなっての鍵であると考えているのであれば、あなたは関係インテリジェンスが必要です。";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"ガブリエル";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Steve";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Fear is the path to the dark side…fear leads to anger…anger leads to hate…hate leads to suffering.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Gabriel";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"リレーションシップ·インテリジェンスは、人々が人々とビジネスを行う、通信や顧客との事の上に滞在し、理解、計画の芸術であると一日の終わりをcolleagues.At 。でも、大企業は、顔のない実体ではありませんが、それらは単に人々のグループで構成されている。あなたが人々を治療する方法より多くの紹介をもっとお得な情報を閉じるとなっての鍵であると考えているのであれば、あなたは関係インテリジェンスが必要です。";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"ガブリエル";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Rejoice for those around you who transform into the Force. Mourn them, do not. Miss them, do not. Attachment leads to jealousy. The shadow of greed, that is. Train yourself to let go of everything you fear to lose.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"George Lucas";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"A Jedi uses the Force for knowledge and defense, never for attack.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Steve";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    dataDictionary = [NSMutableDictionary dictionary];
    
    titleString = @"";
    contentString = @"Fear is the path to the dark side…fear leads to anger…anger leads to hate…hate leads to suffering.";
    dueString = @"";
    sourceString = @"Note";
    authorString = @"Yoda";
    
    [dataDictionary setObject:[NSNumber numberWithInteger:kNote] forKey:@"type"];
    [dataDictionary setObject:titleString forKey:@"title"];
    [dataDictionary setObject:contentString forKey:@"content"];
    [dataDictionary setObject:dueString forKey:@"due"];
    [dataDictionary setObject:sourceString forKey:@"source"];
    [dataDictionary setObject:authorString forKey:@"author"];
    
    [dataArray addObject:dataDictionary];
    
    
    return dataArray;
}
@end
