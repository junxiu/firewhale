//
//  CalendarCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/24/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"
@interface CalendarCollectionViewCell : UICollectionViewCell

@property (nonatomic) JTCalendar *calendar;
@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet JTCalendarContentView *calendarView;
@end
