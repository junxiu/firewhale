//
//  AttachmentActionSheetCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/4/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachmentActionSheetCollectionViewCellDelegate <NSObject>

- (void)onDeleteButtonPressed:(NSInteger)index;

@end

@interface AttachmentActionSheetCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic) id <AttachmentActionSheetCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *playIconImageView;

- (IBAction)onDeleteButtonPressed:(UIButton *)sender;

@end
