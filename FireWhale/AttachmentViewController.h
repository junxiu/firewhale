//
//  AttachmentViewController.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/4/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachmentViewControllerDeleate <NSObject>

- (void)attachmentsDidModified:(NSArray *)newAttachments;

@end

@interface AttachmentViewController : UIViewController

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic) id<AttachmentViewControllerDeleate> delegate;

@end
