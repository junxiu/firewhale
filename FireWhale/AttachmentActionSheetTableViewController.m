//
//  AttachmentActionSheetTableViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/3/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "AttachmentActionSheetTableViewController.h"

@interface AttachmentActionSheetTableViewController ()

@end

@implementation AttachmentActionSheetTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    [cell.contentView viewWithTag:999].layer.cornerRadius = 4;
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    [[tableView cellForRowAtIndexPath:indexPath] viewWithTag:999].transform = CGAffineTransformMakeScale(0.96f, 0.96f);
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [[tableView cellForRowAtIndexPath:indexPath] viewWithTag:999].transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOAD_DROPBOX_BROWSER" object:nil userInfo:nil];
    }
    if (indexPath.row == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOAD_MEDIA_PICKER" object:nil userInfo:nil];
    }
    if (indexPath.row == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOAD_CAMERA" object:nil userInfo:nil];
    }
}

- (IBAction)cancel:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DISMISS_ATTACHMENT_ACTION_SHEET" object:nil userInfo:nil];
}

@end
