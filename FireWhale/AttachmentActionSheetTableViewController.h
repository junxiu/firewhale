//
//  AttachmentActionSheetTableViewController.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/3/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AttachmentActionSheetTableViewControllerDelegate <NSObject>

- (void)didSelectCancel;

@end
@interface AttachmentActionSheetTableViewController : UITableViewController
@property (nonatomic) id <AttachmentActionSheetTableViewControllerDelegate> delegate;
- (IBAction)cancel:(id)sender;
@end
