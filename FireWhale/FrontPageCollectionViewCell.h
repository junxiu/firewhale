//
//  FrontPageCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/23/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrontPageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIView *subContentView;

@end
