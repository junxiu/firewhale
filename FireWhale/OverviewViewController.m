//
//  OverviewViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "OverviewViewController.h"

#import "GroupCollectionViewCell.h"
#import "WorkspaceCollectionViewCell.h"
#import "FrontPageCollectionViewCell.h"

#import "DataSource.h"

@interface OverviewViewController () <UICollectionViewDelegate, UICollectionViewDataSource> {
    BOOL _selected;
    CAGradientLayer *maskLayer;
    NSInteger selectedIndex;
}

@end

@implementation OverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.addButton.layer.cornerRadius = self.addButton.frame.size.height/2;
    
    if (!maskLayer)
    {
        maskLayer = [CAGradientLayer layer];
        
        UIColor *outerColor = [UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:1];
        UIColor *innerColor = [UIColor colorWithRed:46/255.f green:47/255.f blue:59/255.f alpha:0];
        
        maskLayer.colors = [NSArray arrayWithObjects:(id)innerColor.CGColor,
                            (id)innerColor.CGColor, (id)innerColor.CGColor, (id)outerColor.CGColor, nil];
        maskLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                               [NSNumber numberWithFloat:0.2],
                               [NSNumber numberWithFloat:0.65],
                               [NSNumber numberWithFloat:1.0], nil];
        
        maskLayer.bounds = CGRectMake(0, 0,
                                      self.collectionView.frame.size.width,
                                      self.collectionView.frame.size.height);
        maskLayer.anchorPoint = CGPointZero;
        
        [self.collectionView.layer addSublayer:maskLayer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_selected) {
        return 1 + [[[[[DataSource sharedInstance] getWorkspaces] objectAtIndex:selectedIndex - 1] safeObjectForKey:@"groups"] count] + [[[DataSource sharedInstance] getWorkspaces] count];
    }
    return 1 + [[[DataSource sharedInstance] getWorkspaces] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.item == 0) {
        FrontPageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FrontPageCollectionViewCell" forIndexPath:indexPath];
        cell.titleLabel.text = @"Front Page";
        cell.badgeLabel.text = @"7";
        return cell;
    }
    
    if (_selected) {
        GroupCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GroupCollectionViewCell" forIndexPath:indexPath];
        //cell.titleLabel.text = [[[[[DataSource sharedInstance] getWorkspaces] objectAtIndex:selectedIndex-1] safeObjectForKey:@"groups"] objectAtIndex:indexPath.item - selectedIndex];
        cell.titleLabel.text = @"Mr. Ong Peng Tsin";
        cell.badgeLabel.text = @"13";
        cell.subContentView.backgroundColor = [UIColor colorWithRed:0.996 green:0.596 blue:0.149 alpha:1];
        
        cell.subContentView.alpha = 0;
        cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y - 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
        cell.subContentView.transform = CGAffineTransformMakeScale(0.96f, 0.96f);
        
        [UIView animateWithDuration:0.65f
                              delay:(indexPath.item - 2) * 0.05
             usingSpringWithDamping:0.5
              initialSpringVelocity:20
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             cell.subContentView.alpha = 1;
                             cell.subContentView.frame = CGRectMake(cell.subContentView.frame.origin.x, cell.subContentView.frame.origin.y + 20, cell.subContentView.frame.size.width, cell.subContentView.frame.size.height);
                             cell.subContentView.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL finished) {
                         }];
        
        
        
        return cell;
    }
    else {
        WorkspaceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WorkspaceCollectionViewCell" forIndexPath:indexPath];
        return cell;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width - 20, 60);
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:999].transform = CGAffineTransformMakeScale(0.96f, 0.96f);
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:999].transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item == 0) {
        [self close:nil];
        return;
    }
    if (_selected) {
        if (selectedIndex == indexPath.item) {
            _selected = NO;
            [UIView animateWithDuration:0.45f
                                  delay:0.2f
                 usingSpringWithDamping:0.5
                  initialSpringVelocity:20
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:999].backgroundColor = [UIColor colorWithRed:0.212 green:0.216 blue:0.259 alpha:0.95f];
                                 [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:998].transform = CGAffineTransformIdentity;                             } completion:^(BOOL finished) {
                                 }];
            
            [collectionView performBatchUpdates:^{
                for (int i = 0; i < [[[[[DataSource sharedInstance] getWorkspaces] objectAtIndex:selectedIndex - 1] safeObjectForKey:@"groups"] count]; i++) {
                    [collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:i + selectedIndex + 1 inSection:0]]];
                }
            } completion:^(BOOL finished) {
                //
            }];
        }
        else {
            [self close:nil];
        }
    }
    else {
        _selected = YES;
        selectedIndex = indexPath.item;
        [UIView animateWithDuration:0.45f
                              delay:0
             usingSpringWithDamping:0.5
              initialSpringVelocity:20
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:999].backgroundColor = [UIColor colorWithRed:0.996 green:0.596 blue:0.149 alpha:1];
                             [[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:998].transform = CGAffineTransformMakeRotation(M_PI);
                         } completion:^(BOOL finished) {
                         }];
        
        [self performBlock:^{
            [collectionView performBatchUpdates:^{
                for (int i = 0; i < [[[[[DataSource sharedInstance] getWorkspaces] objectAtIndex:selectedIndex - 1] safeObjectForKey:@"groups"] count]; i++) {
                    
                    [collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:i + selectedIndex + 1 inSection:0]]];
                }
            } completion:^(BOOL finished) {
                //
            }];
        } afterDelay:0.05f];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    maskLayer.position = CGPointMake(0, scrollView.contentOffset.y + scrollView.contentInset.top);
    [CATransaction commit];
}
#pragma mark - Methods

- (IBAction)close:(id)sender {
    [UIView animateWithDuration:0.4f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.navBarView setFrameOriginY: - 12];
                         [self.collectionView setFrameOriginY:self.collectionView.frame.origin.y - 12];
                         [self.addButton setFrameOriginY:self.addButton.frame.origin.y - 12];
                     } completion:^(BOOL finished) {
                     }];
    
    
    [UIView animateWithDuration:0.3f
                          delay:0.2f
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.navBarView setFrameOriginY:self.view.frame.size.height];
                         [self.collectionView setFrameOriginY:self.view.frame.size.height + self.navBarView.frame.size.height];
                         [self.addButton setFrameOriginY:self.view.frame.size.height + self.addButton.frame.origin.y];
                         [self.backgroundImageView setAlpha:1];
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.35f
                          delay:0.3f
         usingSpringWithDamping:0.8f
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view viewWithTag:999].alpha = 0;
                     } completion:^(BOOL finished) {
                         [self dismissViewControllerAnimated:NO completion:nil];
                     }];
}
@end
