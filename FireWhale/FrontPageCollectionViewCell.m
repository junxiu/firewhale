//
//  FrontPageCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/23/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "FrontPageCollectionViewCell.h"

@implementation FrontPageCollectionViewCell

- (void)awakeFromNib {
    
    if (self.subContentView.layer.cornerRadius == 0) {
        self.subContentView.layer.cornerRadius = 3;
        self.subContentView.backgroundColor = [UIColor colorWithRed:0.212 green:0.216 blue:0.259 alpha:0.95f];
    }
    
    self.iconImageView.image = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}
@end
