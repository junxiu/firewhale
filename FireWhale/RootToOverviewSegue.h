//
//  RootToOverviewSegue.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootToOverviewSegue : UIStoryboardSegue

@end
