//
//  DocumentActionSheetCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/4/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "DocumentActionSheetCollectionViewCell.h"

@implementation DocumentActionSheetCollectionViewCell

- (void)awakeFromNib {
    self.subContentView.layer.cornerRadius = 4;
    self.deleteButton.layer.cornerRadius = self.deleteButton.frame.size.height/2;
}

- (IBAction)onDeleteButtonPressed:(UIButton *)sender {
    [self.delegate onDocDeleteButtonPressed:sender.tag];
}

@end
