//
//  AttachmentCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/24/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttachmentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *subContentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentImageView;

@end
