//
//  DataSource.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/20/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iOS-UtilitiesClasses.h"

typedef NS_ENUM(NSUInteger, DataType) {
    kTask,
    kEvent,
    kEmail,
    kNote,
    kUpdate,
};

@interface DataSource : NSObject

+ (id)sharedInstance;
- (NSArray *)downloadData;
- (NSArray *)getWorkspaces;
@end
