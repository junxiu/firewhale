//
//  WorkspaceCollectionViewCell.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "WorkspaceCollectionViewCell.h"

@implementation WorkspaceCollectionViewCell

- (void)awakeFromNib {
    if (self.subContentView.layer.cornerRadius == 0) {
        self.subContentView.layer.cornerRadius = 3;
        self.subContentView.backgroundColor = [UIColor colorWithRed:0.212 green:0.216 blue:0.259 alpha:0.95f];
    }
    
    self.arrowImageView.tintColor = [UIColor colorWithWhite:1 alpha:1];
    self.arrowImageView.image = [[UIImage imageNamed:@"arrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}
@end
