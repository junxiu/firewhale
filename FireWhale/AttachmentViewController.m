//
//  AttachmentViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/4/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "AttachmentViewController.h"
#import "AttachmentActionSheetCollectionViewCell.h"
#import "DocumentActionSheetCollectionViewCell.h"
#import "DropboxBrowserViewController.h"
#import "iOS-UtilitiesClasses.h"
#import "JTSImageViewController.h"
#import <STKWebKitViewController/STKWebKitModalViewController.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AttachmentViewController () <UICollectionViewDataSource, UINavigationControllerDelegate, UICollectionViewDelegate, UIImagePickerControllerDelegate, DropboxBrowserDelegate,DBRestClientDelegate,AttachmentActionSheetCollectionViewCellDelegate,DocumentActionSheetCollectionViewCellDelegate> {
    DBMetadata *dbMetaData;
    DBRestClient *_restClient;
    NSMutableArray *metaDatas;
    NSMutableArray *localMediaDatas;
    NSMutableArray *filePaths;
    NSMutableArray *mergedFiles;
}

@property (nonatomic) STKWebKitModalViewController *webVc;

@end

@implementation AttachmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(attachmenteAdded)
                                                 name:@"ATTACHMENTS_ADDED" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadMedia)
                                                 name:@"LOAD_MEDIA_PICKER" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadCamera)
                                                 name:@"LOAD_CAMERA" object:nil];
    
    filePaths = [NSMutableArray array];
    
    mergedFiles = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!localMediaDatas) {
            localMediaDatas = [NSMutableArray array];
        }
        
        for (NSDictionary *dict in localMediaDatas) {
            if ([[[dict safeObjectForKey:@"local"] safeObjectForKey:UIImagePickerControllerReferenceURL] isEqual:[info safeObjectForKey:UIImagePickerControllerReferenceURL]]) {
                [picker dismissViewControllerAnimated:YES completion:^{
                    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"ALL_SELECTED_FILES"];
                }];
                return;
            }
        }
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        
        NSData *fileData;
        
        if ([[info safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
            UIImage *image = [dictionary safeObjectForKey:UIImagePickerControllerOriginalImage];
            fileData = UIImageJPEGRepresentation(image, 1);
            
            if (fileData.length > 1e+7) {
                fileData = UIImageJPEGRepresentation(image, 1 - ((fileData.length - 1e+7) / fileData.length));
            }
            
        }
        else if ([[info safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
            fileData = [NSData dataWithContentsOfURL:[info safeObjectForKey:UIImagePickerControllerMediaURL]];
            
            if (fileData.length > 1e+7) {
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    
                    [picker dismissViewControllerAnimated:YES completion:^{
                        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
                    }];
                    
                    [KVNProgress showErrorWithStatus:@"Video exceeded 10 MB"];
                });
                
                return;
            }
        }
        
        [dictionary setObject:info forKey:@"local"];
        
        if (![localMediaDatas containsObject:dictionary]) {
            [localMediaDatas insertObject:dictionary atIndex:0];
            [mergedFiles insertObject:dictionary atIndex:0];
            [filePaths insertObject:@"" atIndex:0];
        }
        
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            
            [self.delegate attachmentsDidModified:mergedFiles];
            [self.collectionView reloadData];
            [KVNProgress dismiss];
        }];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [KVNProgress showWithStatus:@"Loading"];
        });
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}

#pragma mark - Methods

- (void)loadMedia {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    pickerController.delegate = self;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void)loadCamera {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    pickerController.delegate = self;
    pickerController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    pickerController.videoMaximumDuration = 60;
    pickerController.videoQuality = UIImagePickerControllerQualityTypeMedium;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void)attachmenteAdded {
    mergedFiles = [NSMutableArray array];
    metaDatas = [NSMutableArray array];
    
    NSArray *storedSelectedFiles = [[NSUserDefaults standardUserDefaults] objectForKey:@"ALL_SELECTED_FILES"];
    
    for (NSData *data in storedSelectedFiles) {
        DBMetadata *metaData = ((DBMetadata *)[NSKeyedUnarchiver unarchiveObjectWithData:data]);
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [dictionary setObject:metaData forKey:@"db"];
        [metaDatas insertObject:dictionary atIndex:0];
        [self downloadFile:metaData];
    }
    
    [mergedFiles addObjectsFromArray:localMediaDatas];
    [mergedFiles insertObjects:metaDatas atIndexes:[[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(0, metaDatas.count)]];
    
    [self.delegate attachmentsDidModified:mergedFiles];
    
    [self.collectionView reloadData];
}

- (void)downloadFile:(DBMetadata *)file {
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *localPath = [documentsPath stringByAppendingPathComponent:file.filename];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:localPath error:nil];
    [filePaths insertObject:localPath atIndex:0];
    
    if ([file.icon isEqualToString:@"page_white_picture"]) {
        [[self restClient] loadFile:file.path intoPath:localPath];
    }
}

#pragma mark - DBRestClientDelegate

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)localPath {
    if ([filePaths containsObject:localPath]) {
        [self.delegate attachmentsDidModified:mergedFiles];
        [self.collectionView reloadData];
    }
}

- (void)restClient:(DBRestClient *)client loadFileFailedWithError:(NSError *)error {
    [KVNProgress showErrorWithStatus:error.description];
}

- (void)restClient:(DBRestClient *)restClient loadedSharableLink:(NSString *)link forFile:(NSString *)path {
    [KVNProgress showSuccess];
    self.webVc = [[STKWebKitModalViewController alloc] initWithAddress:link];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [self presentViewController:self.webVc animated:YES completion:nil];
}

- (DBRestClient *)restClient {
    if (!_restClient) {
        _restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        _restClient.delegate = self;
    }
    return _restClient;
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return mergedFiles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DBMetadata *dropboxFile = (DBMetadata *)[[mergedFiles objectAtIndex:indexPath.item] safeObjectForKey:@"db"];
    NSDictionary *mediaInfo = [[mergedFiles objectAtIndex:indexPath.item] safeObjectForKey:@"local"];
    
    if (mediaInfo) {
        AttachmentActionSheetCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttachmentActionSheetCollectionViewCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.deleteButton.tag = indexPath.item;
        cell.playIconImageView.alpha = 0;
        
        NSString *mediaTypeString = [mediaInfo safeObjectForKey:UIImagePickerControllerMediaType];
        if ([mediaTypeString isEqualToString:@"public.movie"]) {
            NSString *videoLink = [mediaInfo objectForKey:@"UIImagePickerControllerMediaURL"];
            MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:(NSURL*)videoLink];
            [player setShouldAutoplay:NO];
            UIImage *imageSel = [player thumbnailImageAtTime:0.1f timeOption:MPMovieTimeOptionNearestKeyFrame];
            [cell.attachmentImageView setImage:imageSel];
            cell.playIconImageView.alpha = 1;
        }
        else if ([mediaTypeString isEqualToString:@"public.image"]) {
            [cell.attachmentImageView setImage:[mediaInfo safeObjectForKey:UIImagePickerControllerOriginalImage]];
        }
        
        cell.activityIndicator.alpha = 0;
        [cell.activityIndicator stopAnimating];
        
        return cell;
    }
    else {
        if ([dropboxFile.icon isEqualToString:@"page_white_picture"]) {
            AttachmentActionSheetCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttachmentActionSheetCollectionViewCell" forIndexPath:indexPath];
            cell.delegate = self;
            cell.deleteButton.tag = indexPath.item;
            cell.playIconImageView.alpha = 0;
            
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *localPath = [documentsPath stringByAppendingPathComponent:dropboxFile.filename];
            cell.attachmentImageView.image = nil;
            
            cell.activityIndicator.alpha = 0;
            [cell.activityIndicator stopAnimating];
            
            if (![UIImage imageWithContentsOfFile:localPath]) {
                cell.activityIndicator.alpha = 1;
                [cell.activityIndicator startAnimating];
            }
            else {
                cell.attachmentImageView.image = [UIImage imageWithContentsOfFile:localPath];
            }
            return cell;
        }
        else {
            DocumentActionSheetCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DocumentActionSheetCollectionViewCell" forIndexPath:indexPath];
            cell.documentIconImageView.image = [UIImage imageNamed:dropboxFile.icon];
            cell.documentNameLabel.text = dropboxFile.filename;
            cell.deleteButton.tag = indexPath.item;
            cell.delegate = self;
            return cell;
        }
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DBMetadata *dropboxFile = (DBMetadata *)[[mergedFiles objectAtIndex:indexPath.item] safeObjectForKey:@"db"];
    NSDictionary *mediaInfo = [[mergedFiles objectAtIndex:indexPath.item] safeObjectForKey:@"local"];
    
    if ([[collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[AttachmentActionSheetCollectionViewCell class]]) {
        AttachmentActionSheetCollectionViewCell *attCell = (AttachmentActionSheetCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        if ([[mediaInfo safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.image"] || ([dropboxFile.icon isEqualToString:@"page_white_picture"] && attCell.activityIndicator.alpha == 0)) {
            JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
            imageInfo.image = attCell.attachmentImageView.image;
            CGRect refRect = [attCell.attachmentImageView convertRect:attCell.attachmentImageView.bounds toView:self.view];
            imageInfo.referenceRect = refRect;
            imageInfo.referenceView = self.view;
            
            JTSImageViewController *imageViewer = [[JTSImageViewController alloc] initWithImageInfo:imageInfo
                                                                                               mode:JTSImageViewControllerMode_Image
                                                                                    backgroundStyle:JTSImageViewControllerBackgroundOption_Blurred];
            
            // Present the view controller.
            [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
        }
        else if ([[mediaInfo safeObjectForKey:UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
            NSString *videoLink = [mediaInfo safeObjectForKey:@"UIImagePickerControllerMediaURL"];
            MPMoviePlayerViewController *player = [[MPMoviePlayerViewController alloc] initWithContentURL:(NSURL*)videoLink];
            [self presentMoviePlayerViewControllerAnimated:player];
        }
    }
    else if ([[collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[DocumentActionSheetCollectionViewCell class]]) {
        NSString *statusString = [NSString stringWithFormat:@"Loading link for '%@' from Dropbox",dropboxFile.filename];
        [KVNProgress showWithStatus:statusString];
        [[self restClient] loadSharableLinkForFile:dropboxFile.path shortUrl:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView cellForItemAtIndexPath:indexPath].transform = CGAffineTransformMakeScale(0.96f, 0.96f);
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    [UIView animateWithDuration:0.65f
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [collectionView cellForItemAtIndexPath:indexPath].transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                     }];
}

#pragma mark - DocumentActionSheetCollectionViewCellDelegate

- (void)onDocDeleteButtonPressed:(NSInteger)index {
    [self onDeleteButtonPressed:index];
}

#pragma mark - AttachmentActionSheetCollectionViewCellDelegate

- (void)onDeleteButtonPressed:(NSInteger)index {
    // Get current file path at index
    
    if ([[[mergedFiles objectAtIndex:index] safeObjectForKey:@"db"] isKindOfClass:[DBMetadata class]]) {
        DBMetadata *dbSelectedFile = (DBMetadata *)[[mergedFiles objectAtIndex:index] safeObjectForKey:@"db"];
        
        // Store NSUserDefaults thing for ALL_SELECTED_FILES
        
        NSMutableArray *dbSelectedFiles = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ALL_SELECTED_FILES"]];
        
        // Unarchive DBMetaData, add file path into array
        
        NSMutableArray *dbSelectedFilesPath = [NSMutableArray array];
        
        for (NSData *data in dbSelectedFiles) {
            NSString *dbFilePath = ((DBMetadata *)[NSKeyedUnarchiver unarchiveObjectWithData:data]).path;
            [dbSelectedFilesPath addObject:dbFilePath];
        }
        
        if ([dbSelectedFilesPath containsObject:dbSelectedFile.path]) {
            [dbSelectedFiles removeObjectAtIndex:[dbSelectedFilesPath indexOfObject:dbSelectedFile.path]];
            [[NSUserDefaults standardUserDefaults] setObject:dbSelectedFiles forKey:@"ALL_SELECTED_FILES"];
        }
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        NSString *mediaFilePath = [[[mergedFiles objectAtIndex:index] safeObjectForKey:@"local"] safeObjectForKey:UIImagePickerControllerReferenceURL];
        for (NSDictionary *storedFilePathDict in localMediaDatas) {
            NSString *storedFilePath = [[storedFilePathDict safeObjectForKey:@"local"] safeObjectForKey:UIImagePickerControllerReferenceURL];
            if ([storedFilePath isEqual:mediaFilePath]) {
                [localMediaDatas removeObjectAtIndex:[localMediaDatas indexOfObject:storedFilePathDict]];
                break;
            }
        }
    }
    
    [self deleteAttachmentAtIndex:index];
}

- (void)deleteAttachmentAtIndex:(NSInteger)index {
    [self.collectionView performBatchUpdates:^{
        [mergedFiles removeObjectAtIndex:index];
        [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:index inSection:0]]];
    } completion:^(BOOL finished) {
        [self.collectionView reloadData];
    }];
    
    [self.delegate attachmentsDidModified:mergedFiles];
}

@end
