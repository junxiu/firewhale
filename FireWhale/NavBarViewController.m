//
//  NavBarViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "NavBarViewController.h"

#import "CalendarViewController.h"
#import "iOS-UtilitiesClasses.h"
#import "RootToCalendarSegue.h"
#import "FXPageControl.h"

@interface NavBarViewController (){
    FXPageControl *pageControl;
}

@end

@implementation NavBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)additionalSetup {
    self.profButton.layer.cornerRadius = CGRectGetHeight(self.profButton.bounds) / 2;
    self.notifButton.layer.cornerRadius = CGRectGetHeight(self.notifButton.bounds) / 2;
    
    self.navBlurView.blurTintColor = [UIColor colorWithRed:29.75/255.f green:30/255.f blue:45/255.f alpha:1];
    pageControl = [[FXPageControl alloc] initWithFrame:CGRectMake(-1, 51.5f, self.view.frame.size.width, 4)];
    [self.view insertSubview:pageControl belowSubview:self.titleButton];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.numberOfPages = 3;
    pageControl.selectedDotColor = [UIColor colorWithRed:0.918 green:0.922 blue:0.925 alpha:1];
    pageControl.dotColor = [UIColor colorWithRed:0.918 green:0.922 blue:0.925 alpha:0.2f];
    pageControl.dotSize = 4.f;
    pageControl.dotSpacing = 5.5f;
    pageControl.defersCurrentPageDisplay = YES;
}

- (IBAction)showSideBar:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MENU_DID_TAPPED" object:nil userInfo:nil];
}

@end
