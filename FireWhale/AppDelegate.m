//
//  AppDelegate.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "AppDelegate.h"
#import "COSTouchVisualizerWindow.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UITextField appearance] setTintColor:[UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1]];
    [[UITextView appearance] setTintColor:[UIColor colorWithRed:0.118 green:0.600 blue:0.933 alpha:1]];
    
    DBSession *dbSession = [[DBSession alloc] initWithAppKey:@"h6z6ou8kehtij5w" appSecret:@"oc0c91kbqlcsykr" root:kDBRootDropbox];
    [DBSession setSharedSession:dbSession];
    
    return YES;
}

- (COSTouchVisualizerWindow *)window
{
    static COSTouchVisualizerWindow *visWindow = nil;
    if (!visWindow) visWindow = [[COSTouchVisualizerWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [visWindow setFillColor:[UIColor colorWithRed:0.906 green:0.290 blue:0.506 alpha:1]];
    [visWindow setStrokeColor:[UIColor colorWithRed:0.906 green:0.290 blue:0.506 alpha:0.3f]];
    [visWindow setTouchAlpha:0.1];
    // Ripple Color
    [visWindow setRippleFillColor:[UIColor yellowColor]];
    [visWindow setRippleStrokeColor:[UIColor purpleColor]];
    [visWindow setRippleAlpha:0.1];
    
    return visWindow;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
            NSLog(@"App linked successfully!");
            // At this point you can start making API calls
        }
        return YES;
    }
    // Add whatever other url handling code your app requires here
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
