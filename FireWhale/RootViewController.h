//
//  RootViewController.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCRBlurView.h"
@interface RootViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIView *navBarContainerView;
@property (weak, nonatomic) IBOutlet UIView *feedContainerView;
- (IBAction)add:(id)sender;

@end
