//
//  RootViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/19/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "RootViewController.h"
#import "iOS-UtilitiesClasses.h"

@interface RootViewController () {
    CGPoint storedPoint;
    BOOL pointStored;
    BOOL isDraggingDown;
}

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
    [self listenToNofications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)additionalSetup {
    self.addButton.layer.cornerRadius = CGRectGetHeight(self.addButton.bounds) / 2;
    self.view.layer.cornerRadius = 3;
}

- (void)listenToNofications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setNavBarOffset:)
                                                 name:@"FEED_DID_SCROLL"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showNavBar)
                                                 name:@"FEED_DID_DRAG"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollViewEndDragging:)
                                                 name:@"SCROLLVIEW_DID_END_DRAGGING"
                                               object:nil];
}

- (void)scrollViewEndDragging:(NSNotification *)infoDict {
    if (!isDraggingDown) {
        if (self.navBarContainerView.frame.origin.y < 0) {
            UIScrollView *scorllView = (UIScrollView *)[infoDict.userInfo safeObjectForKey:@"scrollView"];
            [UIView animateWithDuration:0.6f
                                  delay:0
                 usingSpringWithDamping:1
                  initialSpringVelocity:1
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 [self.navBarContainerView setFrameOriginY:-self.navBarContainerView.frame.size.height];
                                 if (scorllView.contentOffset.y <=0) {
                                     [scorllView setContentOffset:CGPointMake(0, scorllView.contentOffset.y + 64) animated:YES];
                                 }
                             } completion:^(BOOL finished) {
                                 //
                             }];
        }
    }
}

- (void)setNavBarOffset:(NSNotification *)infoDict {
    if (!pointStored) {
        storedPoint = [[infoDict.userInfo objectForKey:@"scrollViewContentOffset"] CGPointValue];
        pointStored = YES;
    }
    CGFloat offset = [[infoDict.userInfo objectForKey:@"scrollViewContentOffset"] CGPointValue].y + -storedPoint.y;
    
    if (-offset > 0) {
        offset = 0;
    }
    isDraggingDown = NO;
    [self.navBarContainerView setFrame:CGRectMake(0, -offset, self.navBarContainerView.frame.size.width, self.navBarContainerView.frame.size.height)];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)showNavBar {
    isDraggingDown = YES;
    pointStored = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [UIView animateWithDuration:0.6f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.navBarContainerView setFrame:CGRectMake(0, 0, self.navBarContainerView.frame.size.width, self.navBarContainerView.frame.size.height)];
                     } completion:^(BOOL finished) {
                         //
                     }];
    
    
}

- (IBAction)add:(id)sender {
}
@end
