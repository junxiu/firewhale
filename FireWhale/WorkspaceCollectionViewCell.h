//
//  WorkspaceCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 11/21/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkspaceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
