//
//  NavigationViewController.m
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/1/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import "NavigationViewController.h"
#import "iOS-UtilitiesClasses.h"

@interface NavigationViewController () <UIGestureRecognizerDelegate> {
    CGFloat _centerX;
    BOOL _menuShown;
    
    BOOL _powerDamp;
}

@end

@implementation NavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self additionalSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Methods

- (void)additionalSetup {
    self.menuView.layer.shadowOffset = CGSizeMake(10, 0);
    self.menuView.layer.shadowOpacity = 0;
    self.menuView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.menuView.layer.shadowRadius = 20;
    
    UIScreenEdgePanGestureRecognizer *leftEdgeGesture = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftEdgeGesture:)];
    leftEdgeGesture.edges = UIRectEdgeLeft;
    leftEdgeGesture.delegate = self;
    [self.view addGestureRecognizer:leftEdgeGesture];
    _centerX = self.view.bounds.size.width / 2;
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showSideView)
                                                 name:@"MENU_DID_TAPPED"
                                               object:nil];
}

#pragma mark - UIGestureRecognizerDelegate

- (void)swipe:(UISwipeGestureRecognizer *)gesture {
    if (_menuShown) {
        [self dismissSideView];
    }
}

- (void)handleLeftEdgeGesture:(UIScreenEdgePanGestureRecognizer *)gesture {
    if (!_menuShown) {
        if (self.menuView.layer.shadowOpacity == 0) {
            CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
            anim.fromValue = [NSNumber numberWithFloat:0];
            anim.toValue = [NSNumber numberWithFloat:0.3];
            anim.duration = 0.35f;
            [self.menuView.layer addAnimation:anim forKey:@"shadowOpacity"];
            self.menuView.layer.shadowOpacity = 0.3;
        }
        
        CGPoint translation = [gesture translationInView:gesture.view];
        if(UIGestureRecognizerStateBegan == gesture.state || UIGestureRecognizerStateChanged == gesture.state) {
            if (translation.x > 240) {
                [self.menuView setFrameOriginX:0];
                CGFloat extra = (translation.x - 240) / 500;
                NSString *value = [NSString stringWithFormat:@"%.3f", extra];
                self.menuView.transform = CGAffineTransformMakeScale(1 + [value floatValue], 1);
                _powerDamp = YES;
            }
            else {
                self.overlayView.alpha = translation.x/240;
                [self.menuView setFrameOriginX:-240 + translation.x];
                CGFloat transform = 1-(translation.x/240 * 0.05f);
                self.rootView.transform = CGAffineTransformMakeScale(transform, transform);
                _powerDamp = NO;
            }
        }
        else {
            [UIView animateWithDuration:0.45f
                                  delay:0
                 usingSpringWithDamping:1
                  initialSpringVelocity:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 if (translation.x > 0) {
                                     self.overlayView.alpha = 1;
                                     self.rootView.transform = CGAffineTransformMakeScale(0.95f, 0.95f);
                                     self.menuView.transform = CGAffineTransformIdentity;
                                     _menuShown = YES;
                                 }
                                 else {
                                     _menuShown = NO;
                                     self.overlayView.alpha = 0;
                                     self.rootView.transform = CGAffineTransformIdentity;
                                 }
                             } completion:^(BOOL finished) {
                             }];
            CGFloat damping = 0;
            CGFloat velocity = 0;
            if (_powerDamp) {
                damping = 0.2f;
                velocity = 8;
            }
            else {
                damping = 0.75f;
                velocity = 20;
            }
            
            [UIView animateWithDuration:0.45f
                                  delay:0.03f
                 usingSpringWithDamping:damping
                  initialSpringVelocity:velocity
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 if (translation.x > 0) {
                                     [self.menuView setFrameOriginX:0];
                                 }
                                 else {
                                     [self.menuView setFrameOriginX:-240];
                                     CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
                                     anim.fromValue = [NSNumber numberWithFloat:self.menuView.layer.shadowOpacity];
                                     anim.toValue = [NSNumber numberWithFloat:0.0];
                                     anim.duration = 1.0;
                                     [self.menuView.layer addAnimation:anim forKey:@"shadowOpacity"];
                                     self.menuView.layer.shadowOpacity = 0.0;
                                 }
                             } completion:^(BOOL finished) {
                             }];
        }
    }
}

- (void)showSideView {
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    anim.fromValue = [NSNumber numberWithFloat:self.menuView.layer.shadowOpacity];
    anim.toValue = [NSNumber numberWithFloat:0.3f];
    anim.duration = 0.35f;
    [self.menuView.layer addAnimation:anim forKey:@"shadowOpacity"];
    self.menuView.layer.shadowOpacity = 0.3f;
    
    [UIView animateWithDuration:0.45f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.overlayView.alpha = 1;
                         self.rootView.transform = CGAffineTransformMakeScale(0.95f, 0.95f);
                         self.menuView.transform = CGAffineTransformIdentity;
                         _menuShown = YES;
                     } completion:^(BOOL finished) {
                     }];
    
    CGFloat damping = 0.8;
    CGFloat velocity = 20;
    
    [UIView animateWithDuration:0.45f
                          delay:0.03f
         usingSpringWithDamping:damping
          initialSpringVelocity:velocity
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.menuView setFrameOriginX:0];
                     } completion:^(BOOL finished) {
                     }];
}

- (void)dismissSideView {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    anim.fromValue = [NSNumber numberWithFloat:self.menuView.layer.shadowOpacity];
    anim.toValue = [NSNumber numberWithFloat:0.0];
    anim.duration = 1.0;
    [self.menuView.layer addAnimation:anim forKey:@"shadowOpacity"];
    self.menuView.layer.shadowOpacity = 0.0;
    
    [UIView animateWithDuration:0.4f
                          delay:0
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.menuView setFrameOriginX:10];
                         self.rootView.transform = CGAffineTransformMakeScale(0.93f, 0.93f);
                     } completion:^(BOOL finished) {
                     }];
    
    
    [UIView animateWithDuration:0.3f
                          delay:0.2f
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.menuView setFrameOriginX:-240];
                     } completion:^(BOOL finished) {
                     }];
    
    [UIView animateWithDuration:0.35f
                          delay:0.3f
         usingSpringWithDamping:0.55f
          initialSpringVelocity:20
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.overlayView.alpha = 0;
                         self.rootView.transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                     }];
    _menuShown = NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return NO;
}

- (IBAction)overlayViewDidTapped:(UITapGestureRecognizer *)sender {
    [self dismissSideView];
}
@end
