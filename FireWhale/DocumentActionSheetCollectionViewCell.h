//
//  DocumentActionSheetCollectionViewCell.h
//  FireWhale
//
//  Created by Jun Xiu Chan on 12/4/14.
//  Copyright (c) 2014 GettingReal. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DocumentActionSheetCollectionViewCellDelegate <NSObject>

- (void)onDocDeleteButtonPressed:(NSInteger)index;

@end

@interface DocumentActionSheetCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *subContentView;
@property (weak, nonatomic) IBOutlet UIImageView *documentIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *documentNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic) id <DocumentActionSheetCollectionViewCellDelegate> delegate;

- (IBAction)onDeleteButtonPressed:(UIButton *)sender;

@end
