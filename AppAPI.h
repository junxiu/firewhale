//
//  AppAPI.h
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "iOS-UtilitiesClasses.h"

#define APP_HOSTNAME @"saleswhale-v2-env-tcqj2t2dst.elasticbeanstalk.com"
#define VERSION @"v1"

@class AFHTTPRequestOperation;

typedef enum {
    AFRequestMethodGet = 1,
    AFRequestMethodPost,
    AFRequestMethodPut,
    AFRequestMethodDelete
} AFRequestMethod;

@interface AppAPI : NSObject

+ (id)sharedInstance;

- (AFHTTPRequestOperation*)requestWithPath:(NSString *)path
                                 parameters:(NSDictionary *)parameters
                                     method:(AFRequestMethod)method
                                 sslEnabled:(BOOL)sslEnabled
                            completionBlock:(void (^)(NSMutableDictionary *responseDict))completionBlock
                                 errorBlock:(void (^)(NSString *errorMessage))errorBlock;

@end
