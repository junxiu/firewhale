//
//  OmuraApiManager.m
//  OmuraAPI
//
//  Created by Jun Xiu Chan on 12/8/14.
//  Copyright (c) 2014 gettingreal. All rights reserved.
//

#import "OmuraApiManager.h"
#import "OmuraFile.h"

@interface OmuraApiManager () {
}

@end
@implementation OmuraApiManager

- (void)createNote:(NSString *)note
   withAttachments:(NSArray *)attachments
           toGroup:(NSInteger)groupID
withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
        errorBlock:(void (^)(NSString *errorMessage))errorBlock {
    
    /*
    {
        "auth_token" : "Bnykjzgsmza3cGMt9qBV",
        "note" : {
            "body" : "This is a note that contains a whale."
        }
    }
    */
    
    [self requestWithPath:[NSString stringWithFormat:@"groups/%li/notes",(long)groupID]
               parameters:@{@"auth_token":[self getUserAuthToken],
                             @"note":@{@"body":note}}
                   method:AFRequestMethodPost
               sslEnabled:NO
          completionBlock:^(NSMutableDictionary *responseDict) {
              //completed
              
              if (attachments.count != 0) {
                  for (NSDictionary *fileDictionary in attachments) {
                      [self attachFile:[fileDictionary safeObjectForKey:@"fileData"]
                          withFileName:[fileDictionary safeObjectForKey:@"fileName"]
                           andFileType:[fileDictionary safeObjectForKey:@"fileType"]
                                toNote:[[[responseDict safeObjectForKey:@"note"] safeObjectForKey:@"id"] integerValue]];
                  }
                  
              }
              
          } errorBlock:^(NSString *errorMessage) {
              //error
          }];
    
}

- (void)attachFile:(NSData *)fileData withFileName:(NSString *)fileName andFileType:(NSString *)fileType toNote:(NSInteger)noteId {
    
    /*
    {
        "auth_token" : "Bnykjzgsmza3cGMt9qBV",
        "asset" : form-data
    }
    */
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@/notes/%li/file_assets",APP_HOSTNAME,VERSION,(long)noteId]]];

    AFHTTPRequestOperation *op = [manager POST:@""
                                    parameters:@{@"auth_token":[self getUserAuthToken]}
                     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                         
        [formData appendPartWithFileData:fileData name:@"asset" fileName:fileName mimeType:fileType];
                         
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    
    [op start];
    
}

- (void)refreshNewsFeedInGroup:(NSInteger)groupID
           withCompletionBlock:(void (^)(NSDictionary *responseDict))completionBlock
                    errorBlock:(void (^)(NSString *errorMessage))errorBlock{
    
    [self requestWithPath:[NSString stringWithFormat:@"groups/%li/notes",(long)groupID]
               parameters:@{@"auth_token":[self getUserAuthToken]}
                   method:AFRequestMethodGet
               sslEnabled:NO
          completionBlock:^(NSMutableDictionary *responseDict) {
              //completed
          } errorBlock:^(NSString *errorMessage) {
              //error
          }];
    
}

- (NSString *)getUserAuthToken {
    return @"Bnykjzgsmza3cGMt9qBV";
}

@end
